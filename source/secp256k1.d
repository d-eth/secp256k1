module secp256k1;

import std.stdio;
import std.exception : enforce;
import std.conv : to, text;
import std : toHexString;
import std.algorithm : reverse;

import include.secp256k1_recovery;
import include.secp256k1;

import keccak : keccak256;

alias bytes = ubyte[];
unittest
{
    writeln("Elliptic Curve example");
    ubyte[32] pk;
    pk[0 .. $] = "beb75b08049e9316d1375999c7d968f3c23fdf606b296fcdfc9a41cdd7e7347c".hexToBytes;
    auto ec = new secp256k1(pk);
    auto msg = keccak256(cast(ubyte[]) "abbbabbaabababababa");
    ec.writeln;
    ec.sign(msg).writeln;
    ec.address.toHexString.writeln(" --  address");
    ec.sign(msg).ecRecover(msg).toHexString.writeln(" -- recovered address");
    foreach (i; 0 .. 500)
    {
        auto sign = ec.sign(msg);
        auto recAddr = sign.ecRecover(msg);
        assert(recAddr == ec.address, text(sign, recAddr));
    }
    assert(ec.verify(msg, ec.sign(msg)));
}

private bytes hexToBytes(string s) pure
{
    import std : chunks, map, array, startsWith, replace;
    import std.range : padLeft;

    s = s.replace("_", "").replace(" ", "");
    if (s.startsWith(`0x`))
        s = s[2 .. $];

    return s.padLeft('0', s.length + s.length % 2).chunks(2).map!q{a.parse!ubyte(16)}.array;
}

struct Signature
{
    int recid;
    ubyte[32] r;
    ubyte[32] s;
}

class secp256k1
{
    private secp256k1_context* ctx;
    bool canSign;
    bool canVerify;
    ubyte[32] secKey;
    ubyte[64] pubKey;

    this() @trusted
    {
        ctx = secp256k1_context_create(SECP256K1_CONTEXT_VERIFY | SECP256K1_CONTEXT_SIGN);
        do
        {
            import random = std.random;

            random.uniform!"[]"(ulong.min, ulong.max);
            static immutable secKeySize = 32 / ulong.sizeof;
            foreach (ref e; cast(ulong[secKeySize]) secKey)
            {
                e = random.uniform(ulong.min, ulong.max);
            }
        }
        while (!secp256k1_ec_seckey_verify(ctx, secKey.ptr));
        canSign = canVerify = true;
        assert(secp256k1_ec_pubkey_create(ctx, cast(secp256k1_pubkey*)&pubKey, secKey.ptr));
    }

    this(const ubyte[32] key) pure nothrow @safe @nogc
    {
        canSign = canVerify = true;
        secKey = key;
        ctx = secp256k1_context_create(SECP256K1_CONTEXT_VERIFY | SECP256K1_CONTEXT_SIGN);
        assert(secp256k1_ec_pubkey_create(ctx, cast(secp256k1_pubkey*)&pubKey, secKey.ptr));
    }

    this(const ubyte[64] pubKey) pure nothrow @safe @nogc
    {
        canVerify = true;
        ctx = secp256k1_context_create(SECP256K1_CONTEXT_VERIFY);
        this.pubKey = pubKey;
    }

    Signature sign(const ubyte[] data) pure @trusted
    {
        ubyte[32] msgHash = keccak256(data);
        return signHash(msgHash);
    }

    Signature signHash(const ubyte[32] msgHash) pure @trusted
    {
        canSign.enforce("this key cannot sign: secKey not inited");
        secp256k1_ecdsa_recoverable_signature signature;
        ctx.secp256k1_ecdsa_sign_recoverable(&signature, msgHash.ptr, secKey.ptr, null, null);

        signature.data[].reverse;
        return Signature(signature.data[0], signature.data[33 .. 65], signature.data[1 .. 33]);
    }

    bool verify(const ubyte[] data, const Signature sig) pure nothrow @nogc @trusted

    {
        secp256k1_ecdsa_signature cSig;

        auto msgHash = keccak256(data);
        cSig.data[32 .. 64] = sig.r;
        cSig.data[0 .. 32] = sig.s;
        cSig.data[].reverse;
        return cast(bool) ctx.secp256k1_ecdsa_verify(&cSig, msgHash.ptr,
                cast(secp256k1_pubkey*)&pubKey);

    }

    ubyte[20] address() @property const pure @nogc @safe nothrow
    {
        return pubKey.secp256k1_pubkey.toAddress;
    }

    ~this()
    {
        secp256k1_context_destroy(ctx);
    }
}

ubyte[20] toAddress(secp256k1_pubkey pubKey) pure nothrow @nogc @safe
{
    import std.algorithm : reverse;

    pubKey.data[0 .. 32].reverse;
    pubKey.data[32 .. $].reverse;
    return keccak256(pubKey.data[])[12 .. $];

}

ubyte[20] ecRecover(Signature sig, ubyte[] msg) pure nothrow @nogc @trusted
{
    auto ctx = secp256k1_context_create(SECP256K1_CONTEXT_VERIFY);
    secp256k1_pubkey pubKey;
    secp256k1_ecdsa_recoverable_signature cSig;
    cSig.data[0] = cast(ubyte) sig.recid;
    cSig.data[33 .. 65] = sig.r;
    cSig.data[1 .. 33] = sig.s;
    cSig.data[].reverse;
    auto msgHash = keccak256(msg);
    ctx.secp256k1_ecdsa_recover(cast(secp256k1_pubkey*)&pubKey, &cSig, msgHash.ptr);
    return toAddress(pubKey);

}

unittest
{
    import std.stdio;

    writeln("Leading zero seach test");
    foreach (i; 0 .. 10_000)
    {
        auto a = new secp256k1;
        auto msg = keccak256(cast(ubyte[]) "abbbabbaabababababa");
        auto sig = a.sign(msg);
        assert(a.verify(msg, sig));
        auto addr = a.address.toHexString;
        if (addr.leadingZeros > 2)
        {
            addr.writeln;
        }
    }

}

auto leadingZeros(T)(T s) pure nothrow @nogc @safe
{

    foreach (i, c; s)
    {
        if (c != '0')
            return i;
    }
    return s.length;
}
